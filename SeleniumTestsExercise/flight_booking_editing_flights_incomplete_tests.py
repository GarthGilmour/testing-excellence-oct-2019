from unittest import TestCase, main

from selenium import webdriver


class FlightBookingAppTest(TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)

    def tearDown(self):
        self.driver.close()

    def test_editing_a_flight_updates_the_listing(self):
        # get the homepage
        self.driver.get("http://localhost:4200")
        # show the table
        # select the first flight
        # Remember its details
        flight_number = "find flight number from selenium"
        # edit the flight, changing its origin or departure time or destination or arrival time
        field_value = "value to save in the form"
        # save the flight

        # verify the updated details are visible on the flight listing
        flight_detail_body = self.driver.find_element_by_tag_name("body").text
        self.assertIn(f"Flight Number: {flight_number}", flight_detail_body)
        self.assertIn(f"Flight Origin: {field_value}", flight_detail_body)


if __name__ == '__main__':
    main()
