from unittest import TestCase, main

from selenium import webdriver


class FlightBookingAppTest(TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)

    def tearDown(self):
        self.driver.close()

    def test_home_page_is_reachable(self):
        self.driver.get("http://localhost:4200")
        self.assertEqual("Flight Booking Application", self.driver.title)

    def test_flights_table_is_viewable(self):
        self.fetch_flights_table()
        table = self.fetch_flights_table()
        self.assertEqual("table", table.tag_name)

    def test_flights_table_lists_flights(self):
        table = self.fetch_flights_table()
        rows = table.find_elements_by_xpath("tbody/tr")
        self.assertEqual(20, len(rows))

    def test_flights_table_contains_flight_numbers(self):
        nested_numbers = (range(101, 106), range(201, 206), range(301, 306), range(401, 406))
        numbers = [num for sublist in nested_numbers for num in sublist]

        table = self.fetch_flights_table()
        rows = table.find_elements_by_xpath("tbody/tr")
        for index in range(0, len(rows)):
            first_cell = rows[index].find_element_by_tag_name("td")
            self.assertEqual(numbers[index], int(first_cell.text))

    def fetch_flights_table(self):
        self.driver.get("http://localhost:4200")
        button = self.driver.find_element_by_id("flights-navigation")
        button.click()
        return self.driver.find_element_by_id("flights-table")



if __name__ == '__main__':
    main()
