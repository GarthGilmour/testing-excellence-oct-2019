# Selenium Testing Flight Booking 

This project contains a simple Python test that exercises the front end
of the Flight Booking application. 

flight_booking_tests.py contains 4 tests which use Selenium to control the 
application in Chrome. 

The tests in the example exercise the flights list table, but not any of the
 subsequent pages in the application.

Your challenge:

Write a test that tests editing flight details. 

Your test should verify that selecting a flight, editing that flight and then
 saving that flight results in the details on the flight details page being
  updated.

There's a single test in flight_booking_editing_flights_incomplete_tests.py 
for you to work with. It has some comments to guide your approach, which you
can safely ignore if you have a different approach you want to take.