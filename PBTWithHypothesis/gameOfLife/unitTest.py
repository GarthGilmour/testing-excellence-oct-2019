import unittest
from gameOfLife.cell import Cell
from unittest.mock import Mock


class CellTest(unittest.TestCase):
    def setUp(self):
        self.__neighbours = [
                                Cell([]),
                                Cell([]),
                                Cell([]),
                                Cell([]),
                                Cell([]),
                                Cell([]),
                                Cell([]),
                                Cell([])
                            ]

    def test_is_dead_by_default(self):
        cell = Cell(self.__neighbours)
        self.assertEquals(Cell.DEAD, cell.state())

    def test_fails_with_too_many_neighbours(self):
        self.__neighbours.append(Cell([]))
        self.assertRaises(RuntimeError, lambda: Cell(self.__neighbours))

    def test_can_be_made_alive(self):
        cell = Cell(self.__neighbours)
        self.assertEquals(Cell.DEAD, cell.state())
        cell.make_alive()
        self.assertEquals(Cell.ALIVE, cell.state())

    def test_can_be_made_dead(self):
        cell = Cell(self.__neighbours)
        self.assertEquals(Cell.DEAD, cell.state())
        cell.make_alive()
        self.assertEquals(Cell.ALIVE, cell.state())
        cell.make_dead()
        self.assertEquals(Cell.DEAD, cell.state())

    def test_str_works(self):
        cell = Cell(self.__neighbours)
        self.assertEquals("A cell which is dead", str(cell))
        cell.make_alive()
        self.assertEquals("A cell which is alive", str(cell))

    def test_becomes_alive_with_three_live_neighbours(self):
        for x in range(0,3):
            self.__neighbours[x].make_alive()

        cell = Cell(self.__neighbours)
        cell.change_state()
        self.assertEquals(Cell.ALIVE, cell.state())

    def test_stays_alive_with_two_live_neighbours(self):
        cell = Cell(self.__neighbours)
        cell.make_alive()

        self.__neighbours[0].make_alive()
        self.__neighbours[1].make_alive()

        cell.change_state()
        self.assertEquals(Cell.ALIVE, cell.state())

    def test_stays_alive_with_three_live_neighbours(self):
        cell = Cell(self.__neighbours)
        cell.make_alive()

        for x in range(0,3):
            self.__neighbours[x].make_alive()

        cell.change_state()
        self.assertEquals(Cell.ALIVE, cell.state())

    def test_starves_with_one_live_neighbour(self):
        cell = Cell(self.__neighbours)
        cell.make_alive()

        self.__neighbours[0].make_alive()

        cell.change_state()
        self.assertEquals(Cell.DEAD, cell.state())

    def test_starves_with_no_live_neighbours(self):
        cell = Cell(self.__neighbours)
        cell.make_alive()

        cell.change_state()
        self.assertEquals(Cell.DEAD, cell.state())

    def test_is_crowded_out_with_four_or_more_live_neighbours(self):
        def reset_neighbours():
            for cell in self.__neighbours:
                cell.make_dead()

        for x in range(4,8):
            cell = Cell(self.__neighbours)
            cell.make_alive()

            reset_neighbours()
            for y in range(0, x):
                self.__neighbours[x].make_alive()

            cell.change_state()
            self.assertEquals(Cell.DEAD, cell.state())

    def test_stays_dead_unless_three_neighbours_are_alive(self):
        for x in range(0,8):
            for y in range(0,x):
                self.__neighbours[x].make_alive()

            cell = Cell(self.__neighbours)
            cell.change_state()

            if not x == 3:
                self.assertEquals(Cell.DEAD, cell.state())

    def test_change_state_polls_all_neighbours(self):
        for x in range(0,8):
            self.__neighbours[x] = Mock()
            self.__neighbours[x].state = Mock(return_value=Cell.DEAD)

        cell = Cell(self.__neighbours)
        cell.change_state()

        for cell in self.__neighbours:
            self.assertEquals(1, cell.state.call_count)


if __name__ == '__main__':
    unittest.main()
