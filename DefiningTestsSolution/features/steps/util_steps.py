from behave import *


@then('an exception is thrown')
def t2(context):
    assert context.current_exception is not None
    assert isinstance(context.current_exception, Exception)
