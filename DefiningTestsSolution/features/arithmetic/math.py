class MathError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class Math:
    def __init__(self, no1, no2):
        self.no1 = no1
        self.no2 = no2

    def __str__(self):
        return f"Math object holding {self.no1:d} and {self.no2:d}"

    def add(self):
        return self.no1 + self.no2

    def subtract(self):
        return self.no1 - self.no2

    def multiply(self):
        return self.no1 * self.no2

    def divide(self):
        if self.no2 == 0:
            raise MathError('Cant divide by zero')
        return self.no1 / self.no2
