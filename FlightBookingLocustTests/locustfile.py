from locust import HttpLocust, TaskSet, task


class UserBehavior(TaskSet):
    def on_start(self):
        print("Locust task starting")

    def on_stop(self):
        print("Locust task stopping")

    @task(4)
    def flights_by_destination(self):
        self.client.get("/flights/destination/Birmingham")

    @task(3)
    def flights_by_origin(self):
        self.client.get("/flights/origin/Dublin")

    @task(2)
    def single_flight(self):
        self.client.get("/flights/101")

    @task(1)
    def all_flights(self):
        self.client.get("/flights")


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
