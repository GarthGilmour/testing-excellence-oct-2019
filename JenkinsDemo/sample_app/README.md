# Jenkins and Docker and ECS Demo

A sample Jenkinsfile copied from a demo project.
There's some stuff hard-coded here to make the demo easier which should be environment variables (ECR repository IDs)

The deploy stage will break for you without significant work. It's tagging
and pushing the images to ECR then deploying them to ECS.

From a testing perspective, the fun stuff is in the 'Run Tests' stage.

The *docker-compose.yml* file shows how the tests run. It's got three containers,
 * the system under test (SUT)
 * the test suite
 * a chrome headless image

The tests will run against the SUT executing the tests in Headless Chrome via Selenium.

```python

build_number = os.environ['BUILD_NUMBER']

def get_driver():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    capabilities = webdriver.DesiredCapabilities.CHROME
    
    driver = webdriver.Remote(
        command_executor=f"http://selenium-b{build_number}:4444/wd/hub",
        options=options,
        desired_capabilities=capabilities
    )
    return driver

def test_check_homepage_title():
    driver = get_driver()
    driver.get(f"my-app:B${build_number}")

    assert "My First Web Application" in driver.title
    
    driver.close()
```