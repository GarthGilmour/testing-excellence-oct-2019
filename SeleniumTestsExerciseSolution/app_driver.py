class AppDriver:
    def __init__(self, driver, root_url="http://localhost:4200"):
        self.driver = driver
        self.root_url = root_url

    def navigate_to_homepage(self):
        self.driver.get(self.root_url)

    def navigate_to_flights_list(self):
        self.driver.get(f"{self.root_url}/flights")

    def navigate_to_flight_details(self, flight_number):
        self.driver.get(f"{self.root_url}/flights/{flight_number}")

    def browse_to_flight_update_form(self, flight_number):
        self.navigate_to_flight_details(flight_number)
        self.driver.find_element_by_xpath('//button[text()="Edit"]').click()

    def get_flights_table(self):
        button = self.driver.find_element_by_id("flights-navigation")
        button.click()
        return self.driver.find_element_by_id("flights-table")

    def get_first_flight_number(self):
        self.navigate_to_flights_list()
        first_flight = self.driver.find_element_by_css_selector('#flights-table tbody tr')
        return first_flight.find_element_by_class_name("number").text

    def edit_flight_number(self, flight_number, field_target_css, field_value):
        self.browse_to_flight_update_form(flight_number)
        origin_input = self.driver.find_element_by_css_selector(field_target_css)
        origin_input.clear()
        origin_input.send_keys(field_value)
        self.driver.find_element_by_xpath('//input[@value="Update Flight"]').click()

    def get_body_text(self):
        return self.driver.find_element_by_tag_name("body").text

    def get_form_field_value(self, form_field_css):
        return self.driver.find_element_by_css_selector(form_field_css).get_attribute("value")
