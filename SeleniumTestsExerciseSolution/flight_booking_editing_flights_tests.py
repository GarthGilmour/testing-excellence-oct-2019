from unittest import TestCase, main

from selenium import webdriver


class FlightBookingAppTest(TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)

    def tearDown(self):
        self.driver.close()

    def test_editing_a_flight_updates_the_listing(self):
        '''
        This is a simple solution which demonstrates how to browse to and edit the flight
        But it's untidy and reasonably brittle - the test knows too much about the implementation details
        '''
        # get the homepage
        self.driver.get("http://localhost:4200")
        # show the table
        button = self.driver.find_element_by_id("flights-navigation")
        button.click()
        table = self.driver.find_element_by_id("flights-table")
        # select the first flight
        first_flight = table.find_element_by_xpath("tbody/tr")
        # Remember its details
        number = first_flight.find_element_by_class_name("number").text
        select_button = first_flight.find_element_by_tag_name("button")
        select_button.click()
        # edit the flight, changing its origin, departure time, destination and arrival time
        self.assertIn(f"Flight Number: {number}", self.driver.find_element_by_tag_name("body").text)
        edit_button = self.driver.find_element_by_xpath('//button[text()="Edit"]')
        edit_button.click()
        number_input = self.driver.find_element_by_css_selector("input.number").get_attribute("value")
        self.assertEqual(number, number_input)
        origin_input = self.driver.find_element_by_css_selector("input.origin")
        new_origin = "Timbuktu"
        origin_input.clear()
        origin_input.send_keys(new_origin)
        # save the flight
        save_button = self.driver.find_element_by_xpath('//input[@value="Update Flight"]')
        save_button.click()
        # verify the updated details are visible on the flight listing
        flight_detail_body = self.driver.find_element_by_tag_name("body").text
        self.assertIn(f"Flight Number: {number}", flight_detail_body)
        self.assertIn(f"Flight Origin: {new_origin}", flight_detail_body)

if __name__ == '__main__':
    main()
