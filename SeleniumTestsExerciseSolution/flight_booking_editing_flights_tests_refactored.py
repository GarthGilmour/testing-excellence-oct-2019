from unittest import TestCase, main

from selenium import webdriver
from app_driver import AppDriver


class FlightBookingAppTest(TestCase):
    '''
    This solution moves the detail of the page structure out into a separate AppDriver class.
    As a result the test is smaller and easier to read at the expense of needing to maintain the separate custom driver
    '''

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)
        self.app = AppDriver(self.driver)

    def tearDown(self):
        self.driver.close()

    def test_editing_a_flight_updates_the_listing(self):
        # get the homepage
        field_value = "Timbuktu"
        field_target = "input.origin"

        flight_number = self.app.get_first_flight_number()

        self.app.navigate_to_flight_details(flight_number)
        self.assertIn(f"Flight Number: {flight_number}", self.app.get_body_text())

        self.app.browse_to_flight_update_form(flight_number)
        self.assertEqual(flight_number, self.app.get_form_field_value("input.number"))

        self.app.edit_flight_number(flight_number, field_target, field_value)

        flight_detail_body = self.app.get_body_text()
        self.assertIn(f"Flight Number: {flight_number}", flight_detail_body)
        self.assertIn(f"Flight Origin: {field_value}", flight_detail_body)


if __name__ == '__main__':
    main()
