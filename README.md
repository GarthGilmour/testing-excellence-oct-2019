# Testing Excellence

These are the examples and exercises for the **Testing Excellence** course delivering at Instil on 7th October 2019

The accompanying manual [can be downloaded from here](https://drive.google.com/file/d/1i06cz9iBoVzbyV1ZBbtb_6zdLE91ItZm/view?usp=sharing).


